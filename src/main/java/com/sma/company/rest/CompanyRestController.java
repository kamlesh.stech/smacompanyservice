package com.sma.company.rest;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sma.company.entity.Company;
import com.sma.company.service.ICompanyService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/company")
public class CompanyRestController {
	
	@Autowired
	private ICompanyService companyService;
	
	@PostMapping("/create")
	public ResponseEntity<String> createCompany(@RequestBody Company company){
		log.info("ENTER INTO COMPANY CREATE METHOD");
		ResponseEntity<String> resp = null;
		try {
			Long id = companyService.createCompany(company);
			String messgae = "company '"+id+"' created success";
			//resp = new ResponseEntity<String>(messgae,HttpStatus.OK);
			resp = ResponseEntity.ok(messgae);
			
			log.info("COMPANY CREATED {}.",id); 
			
		}catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage()); 
		}
		log.info("ABOUT TO LEAVE COMPANY CREATE METHOD"); 
		return resp;
	}
	
	@GetMapping("/fetch/{id}")
	public ResponseEntity<Company> getOneCompany(@PathVariable("id") Long companyId) {
		log.info("ENTER INTO FETCH ONE COMPANY METHOD");
		ResponseEntity<Company> resp = null;
		try {
			Company c = companyService.getOneCompany(companyId);
			resp = ResponseEntity.ok(c);
			log.info("FETCH ONE METHOD IS SUCCESS");
		} catch (Exception e) {
			//e.printStackTrace();
			log.error(e.getMessage());
			
			throw e;
		}
		log.info("ABOUT TO LEAVE FETCH ONE COMPANY METHOD");
		
		return resp; 
	}
/**	
	public ResponseEntity<String> updateCompany(@RequestBody Company company){
		log.info("ENTER INTO UPDATE COMPANY METHOD");
		ResponseEntity<String> resp = null;
		try {
			companyService.updateCompany(company);
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return null;	
	}
*/	
	@GetMapping("/all")
	public ResponseEntity<List<Company>> getAllCompanies(){
		log.info("ENTER INTO GET ALL COMPANIES METHOD");
		ResponseEntity<List<Company>> resp = null;
		try {
			List<Company> companyList= companyService.getAllCompanies();
			resp = ResponseEntity.ok(companyList);
			log.info("ALL COMPANIES FETCHED SUCCESS"); 
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage()); 
		}

		log.info("ABOUT TO LEAVE GET ALL COMPANIES METHOD");
		
		return resp;
	}
  
}































































