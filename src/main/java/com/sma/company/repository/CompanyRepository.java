package com.sma.company.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sma.company.entity.Company;

public interface CompanyRepository extends JpaRepository<Company, Long>{

}
