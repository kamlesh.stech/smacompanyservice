package com.sma.company.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sma.company.entity.Company;
import com.sma.company.exception.CompanyNotFoundException;
import com.sma.company.repository.CompanyRepository;
import com.sma.company.service.ICompanyService;

@Service
public class CompanyService implements ICompanyService{

	@Autowired
	private CompanyRepository companyRepo;
	
	public Long createCompany(Company company) {
		
		return companyRepo.save(company).getId();
	}

	public Company getOneCompany(Long companyId) {
		Optional<Company> opt = companyRepo.findById(companyId);
		if(opt.isEmpty()) {
			throw new CompanyNotFoundException("Company '"+companyId+"' Not Found !!");
		}else {
			return opt.get();
		}
	}

	public void updateCompany(Company company) {
		if(company.getCregId()!=null && companyRepo.existsById(company.getId())) { 			
			companyRepo.save(company);
		}
	}

	public List<Company> getAllCompanies() {
		return companyRepo.findAll();
	}
	
	
}
