package com.sma.company.service;

import java.util.List;

import com.sma.company.entity.Company;

public interface ICompanyService {
	
	Long createCompany(Company company);
	Company getOneCompany(Long companyId);
	void updateCompany(Company company);
	List<Company> getAllCompanies();
}
